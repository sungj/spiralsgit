var express = require('express');
var Busboy = require('busboy');
var path = require('path');
var fs = require('fs');
var request = require('request');
var debug = require('debug')('lines:router');
var router = express.Router();

var db = loadFile('db.json');
var ex = null;
loadEx();

setInterval(writeDB, 300000); // save every 5 minute
setInterval(loadEx, 3540000); // refresh ex every 59 minutes

var clientID = '3df429eb652c6d3'; //'6a5400948b3b376';

router.get('/', function(req, res, next) {
	res.set('Cache-Control', 'public, max-age=31536000');
	res.render('index', {});
});

router.get('/about', function(req, res, next) {
	res.set('Cache-Control', 'public, max-age=31536000');
	res.render('about', {});
});

router.get('/i', function(req, res, next) {
	res.set('Cache-Control', 'public, max-age=3600');
	res.status(200).send(ex);
});

router.get('/p/*', function(req, res, next) {
	var size = 0;
	var r = request.get(req.path.slice(3));
	r.pipefilter = function(response, dest) {
		response.on('data', function(chunk) {
			size += chunk.length;
			if(size >= 5e6) {
				res.status(400);
				r.abort();
			}
		});
	};
	r.pipe(res);
});

router.post('/u', function(req, res, next) {
	var fields = {imgur: null, finished: false};
	var busboy = new Busboy({headers: req.headers, limits: {files: 2, fields: 5, fieldSize: 100, fileSize: 5000000}});
	var blobSize = null;
	var lf = null;
	busboy.on('file', function(fieldname, file, filename, encoding, mimetype) {
		if(fieldname == 'lines') {
			lf = file;
			writeLF(fields, lf);
		}
		if(fieldname == 'image') {
			var formData = {
				image: {
					value: file,
					options: {
						contentType: 'image/png',
						knownLength: blobSize
					}
				},
				description: 'Created with http://linify.me',
				type: 'binary'
			}
			var options = {
				//url: 'https://api.imgur.com/3/image',
				url: 'https://imgur-apiv3.p.mashape.com/3/image',
				headers: {
					'Authorization': 'Client-ID ' + clientID,
					'X-Mashape-Key': "QbAFBjLfOYmshuzfXOqAWKcd9hOEp1SB2r9jsnAJnYD96F28Xi"
				},
				formData: formData
			}
			request.post(options, function(err, httpResponse, body) {
				if(err) {
					debug(err);
					var e = new Error('Imgur Error');
					e.status = 500;
					next(e);
				}
				try{
					var parsed = JSON.parse(body);
					if(parsed.success == true) {
						fields.imgur = parsed.data.id;
						writeLF(fields, lf);
						addToDB(fields, res);
					} else {
						debug(parsed);
						var e = new Error('Imgur Error');
						e.status = 500;
						next(e);
					}
				} catch(err) {
					debug(err);
					var e = new Error('Imgur Error');
					e.status = 500;
					next(e);
				}
			});			
		}
		
	});
	busboy.on('field', function(fieldname, val, fieldnameTruncated, valTruncated) {
		if(isNaN(val)) {
			var err = new Error('Invalid Form Value');
			err.status = 400;
			next(err);
		} else {
			if(fieldname == 'size') {
				blobSize = val;
			} else {
				fields[fieldname] = val;
			}
		}
	});
	busboy.on('finish', function() {
		fields.finished = true;
		addToDB(fields, res);
	});
	req.pipe(busboy);
});

router.get('*', function(req, res, next) {
	var lf = req.path.slice(1);
	if(/^[a-z0-9]+$/i.test(lf)) { // test alphanumeric
		if(db[lf] != undefined) {
			var fields = db[lf];
			fs.readFile(path.join(__dirname, '..', 'data', lf.slice(0,1), lf + '.lf'), 'base64', function(err, data) {
				if(err) {
					debug(err)
					var err = new Error('LineFile Read Error');
					err.status = 500;
					next(err);
				} else {
					res.set('Cache-Control', 'public, max-age=31536000');
					res.render('rec', {lines: data, lineStep: fields.l, width: fields.w, height: fields.h, add: fields.a, imgur: lf});
				}
			});
		} else {
			next();
		}
	} else {
		next();
	}
});

/* blocks, but only should run once when server is booted */
function loadFile(name) {
	try{
		return JSON.parse(fs.readFileSync(path.join(__dirname, '..', name), 'utf8'));
	} catch(err) {
		debug(err);
	}
}

function loadEx() {
	fs.readFile(path.join(__dirname, '..', 'ex.json'), 'utf8', function(err, data) {
		if(err) {
			debug(err);
		} else {
			try{
				ex = data;
			}catch(e) {
				debug(e);
			}
		}
	});
}

function writeDB() {
	fs.writeFile(path.join(__dirname, '..', 'db.json'), JSON.stringify(db), function(err) {
		if(err)
			debug(err);
	});
}

function writeLF(fields, lf) {
	if(fields.imgur != null && lf != null) {
		lf.pipe(fs.createWriteStream(path.join(__dirname, '..', 'data', fields.imgur.slice(0,1), fields.imgur + '.lf')));
	}
}

function addToDB(fields, res) {
	if(fields.imgur != null && fields.finished == true) {
		db[fields.imgur] = {l: fields.lineStep, w: fields.width, h: fields.height, a: fields.add};
		//writeDB();	
		res.status(200).send(fields.imgur);
	}
}

module.exports = router;
extends layout

block header
	title= 'About - Linify'
block create
	a#create.button(href='/') Create Your Own!
block content
	#aboutText
		h1 About
		div
			div
				h2 What does it do?
			div
				p Linify programmatically converts any image into a drawing made completely of straight full-length lines. Click any of the images to see them reconstructed. Linify was inspired by <a href="https://www.reddit.com/r/Art/comments/454joy/drawing_experiment_every_line_goes_through_the/">this reddit post</a>.
				.iWrap
					a(href='/IEno9Ht')
						img.example(src= 'http://imgur.com/IEno9Ht.png')
			div
				h2 Who made it?
			div
				p Email: linifyme at gmail dot com
				p Twitter: <a href="http://twitter.com/linifyme">@LINIFYme</a>
				.iWrap
					a.example(href='/LNHvhuE')
						img.example(src="http://imgur.com/LNHvhuE.png", alt="Josh")
		h1 Technical
		div
			div
				h2 How does it work?
			div
				p Linify uses a greedy randomized algorithm. First, the darkest pixel is found. Then a number of random lines are drawn through that point, and the pixel values along each line are added together. The line with the darkest average is chosen, and the value of that line is subtracted from the image. Then the whole process is started over again for however many lines we are drawing. By the nature of the algorithm, the optimal line drawing configuration is probably not achieved, but performance is far superior to the naive method of checking every line.
			div
				h2 How are the images stored?
			div
				p To store Linify images in a compact form for replayability, every line drawn for each image has to be stored. This is accomplished by numbering the edges of the image 0-2047 for a 512x512 image. Thus a line can be expressed as 2 11-bit numbers (2^11 = 2048). Including 2 bits for color (gray, red, green, blue), a complete line can be encoded 24 bits. To be able to produce displayable images for thumbnails and social media, each saved image is uploaded to Imgur.
			div
				h2 Tech used
			div
				p Linify uses icons from <a href="http://fortawesome.github.io/Font-Awesome/">Font Awesome</a>. Imgur is used to store images. <a href="http://crossorigin.me">crossorigin.me</a> is used as a CORS proxy. The backend uses a combination of Node.js and Nginx.